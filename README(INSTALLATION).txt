INSTALLATION
------------

### Composer
How to add a composer to your PC you can look under the link https://getcomposer.org/doc/00-intro.md

In the console execute the command

php composer install

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```


import file parser_test.sql to db