<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Products".
 *
 * @property int $id
 * @property string $link
 * @property string $product_name
 * @property string $image
 * @property double $price
 * @property double $shipping_price
 * @property double $percent_price
 * @property string $date_add
 * @property string $date_update
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'product_name', 'image', 'price', 'shipping_price', 'percent_price'], 'required'],
            [['link', 'product_name', 'image'], 'string'],
            [['price', 'shipping_price', 'percent_price'], 'number'],
            [['date_add', 'date_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'product_name' => 'Product Name',
            'image' => 'Image',
            'price' => 'Price',
            'shipping_price' => 'Shipping Price',
            'percent_price' => 'Percent Price',
            'date_add' => 'Date Add',
            'date_update' => 'Date Update',
        ];
    }
}
