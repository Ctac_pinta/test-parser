<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Product parser';
?>

<div class="site-index">
    <div class="curs_block">
        <span>Курс</span>
        <span>1 UAH = <?=round($uah_curs,2)?> RUB</span>
        <span>1 UAH = <?=round($uah_curs/$usd_curs,2)?> USD</span>
        <span>1 UAH = <?=round($uah_curs/$eur_curs,2)?> EUR</span>
        <span class="pull-right"><a href="<?=Url::to(['admin/settings']);?>">settings</a></span>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm($action = '/', $method = 'post', ['class' => 'form-horizontal','id'=>'parce_form', 'data-toggle'=>"validator",'role'=>"form"]) ?>

    <div class="form-group">
        <label>Ссылка товара который нужно спарсить</label>
        <?= Html::input('url', 'link', $link, [
            'class' => 'form-control',
            'id' => 'url',
            'placeholder' => 'Ссылка товара который нужно спарсить',
            'required'=>'required']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Parse And Save', ['class' => 'btn btn-primary', 'name' => 'parser-button']) ?>   
    </div>
    <?= Html::endForm() ?>
    <?php if ($parser_not_found){ ?>
        <div class="alert alert-danger fade in alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            Медот для обработки текущей ссылки не найден.
        </div>
    <?php }?>
    <div class="products_block">
        <h2>Блок добавленых товаров</h2>
        <?php foreach($products as $p){ ?>
            <div class="col-md-3 margin">
                <div class="image col-md-12">
                    <?php if (!empty($p->image)) { ?>
                        <img src="<?=$p->image?>"/>
                    <?php } ?>
                </div>
                <?php if (!empty($p->product_name)) { ?>
                    <div class="name col-md-12">
                        <?=mb_substr ($p->product_name,0,80)?>...
                    </div>
                <?php } ?>
                <?php if (!empty($p->percent_price)) { ?>
                    <div class="price col-md-12">
                        <div class="text-center">Цена с наценкой</div>
                        <div class="col-md-6">
                            <?=round($p->percent_price,2)?> $
                        </div>
                        <div class="col-md-6">
                            <?php if($usd_curs&&$uah_curs){ ?>
                                <?=round($p->percent_price*$usd_curs/$uah_curs,2) ?> UAH
                            <?php } ?>
                        </div>

                    </div>
                <?php } ?>
                <?php if (!empty($p->price)) { ?>
                    <div class="price col-md-12">
                        <div class="text-center">Цена без наценки</div>
                        <div class="col-md-6">
                            <?=round($p->price,2)?> $
                        </div>
                        <div class="col-md-6">
                            <?php if($usd_curs&&$uah_curs){ ?>
                                <?=round($p->price*$usd_curs/$uah_curs,2) ?> UAH
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<style>
.products_block .image{
    height:250px;
}
.products_block .image img{
    width: 100%;
}
.products_block .name{
    height:60px;
}
.products_block .margin{
    margin: 10px 0;
    height: 450px;
}
</style>

