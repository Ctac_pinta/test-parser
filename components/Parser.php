<?php

namespace app\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use app\api\CBRAgent;

class Parser extends Component
{
    /**
     * parsing product from site Amazon.
     *
     * @param object $doc. dom object page what we parse
     * @param float $percent. percent to need plus to product price(%)
     * @param array $data. array of fields to need save to db
     *
     * @return array $data product array fields
     */

    public function parserAmazon($doc, $percent, $link)
    {
        $product_name = $doc->find('#productTitle')->html();
        $product_name = preg_replace("/\s{2,}/", " ", $product_name);
        $img = $doc->find('#imgTagWrapperId img')->attr('src');
        $price = $doc->find('#priceblock_ourprice')->html();
        $price = preg_replace('~[^\d\.]+~', NULL, $price);
        $doc->find('#ourprice_shippingmessage a')->remove();
        $shipping_price = $doc->find('#ourprice_shippingmessage')->html();
        $shipping_price = preg_replace('~[^\d\.]+~', NULL, $shipping_price);
        if ($shipping_price = '.') {
            $shipping_price = 0;
        }
        $priceByPercent = !empty($price)?($price + $price * $percent / 100 + $shipping_price):0;
        $data = [
            'link' => $link,
            'img' => $img,
            'price' => $price,
            'product_name' => $product_name,
            'shipping_price' => $shipping_price,
            'priceByPercent' => $priceByPercent
        ];
        return $data;
    }
    /**
     * parsing product from site Ebay.
     *
     * @param object $doc. dom object page what we parse
     * @param float $percent. percent to need plus to product price(%)
     * @param array $data. array of fields to need save to db
     *
     * @return array $data product array fields
     */
    public function parserEbay($doc, $percent, $link)
    {
        $doc->find('#itemTitle .g-hdn')->remove();
        $product_name = $doc->find('#itemTitle')->html();
        $img = $doc->find('#icImg')->attr('src');
        if ($doc->find('#convbinPrice')->html()) {
            $doc->find('#convbinPrice span')->remove();
            $price = $doc->find('#convbinPrice')->html();
        } else {
            $price = $doc->find('#prcIsum')->attr('content');
        }
        $price = preg_replace('~[^\d\.]+~', NULL, $price);

        if ($doc->find('#isum-shipCostDiv')->html()) {
            $shipping_price = $doc->find('#isum-shipCostDiv')->html();
            $shipping_price = preg_replace('~[^\d\.]+~', NULL, $shipping_price);
        } else {
            $shipping_price = 0;
        }
        $priceByPercent = !empty($price)?($price + $price * $percent / 100 + $shipping_price):0;
        $product = Products::find()->where(['link' => $link])->one();
        $data = [
            'link' => $link,
            'img' => $img,
            'price' => $price,
            'product_name' => $product_name,
            'shipping_price' => $shipping_price,
            'priceByPercent' => $priceByPercent
        ];
        return $data;
    }
    /**
     * parsing product from site Aliexpress.
     *
     * @param object $doc. dom object page what we parse
     * @param float $percent. percent to need plus to product price(%)
     * @param array $data. array of fields to need save to db
     *
     * @return array $data product array fields
     */
    public function parserAliexpress($doc, $percent, $link)
    {
        $cbr = new CBRAgent();
        if ($cbr->load()) {
            $usd_curs = $cbr->get('USD');
            $product_name = $doc->find('.product-name')->html();

            $img = $doc->find('.ui-image-viewer-thumb-frame img')->attr('src');

            if ($doc->find('#j-sku-discount-price')->html()) {
                $price = $doc->find('#j-sku-discount-price')->html();
            } else {
                $price = $doc->find('#j-sku-price')->html();
            }

            $price = str_replace(',', '.', str_replace(' ', '', strip_tags($price)));

            if (stristr($price, '-', true)) {
                $price = stristr($price, '-', true);
            }
            $price = preg_replace('~[^\d\.]+~', NULL, $price);

            $price = !empty($price)?round($price / $usd_curs, 2):0;

            $shipping_price = 0;

            $priceByPercent = !empty($price)?($price + $price * $percent / 100 + $shipping_price):0;
            $data = [
                'link' => $link,
                'img' => $img,
                'price' => $price,
                'product_name' => $product_name,
                'shipping_price' => $shipping_price,
                'priceByPercent' => $priceByPercent
            ];
            return $data;
        }
    }

    /**
     * parsing product from site Tao.
     *
     * @param object $doc. dom object page what we parse
     * @param float $percent. percent to need plus to product price(%)
     * @param array $data. array of fields to need save to db
     *
     * @return array $data product array fields
     */
    public function parserTao($doc, $percent, $link)
    {
		
        $cbr = new CBRAgent();
        if ($cbr->load()) {
            $usd_curs = $cbr->get('USD');
            $uah_curs = $cbr->get('UAH') / 10;

            $img = $doc->find('#content__foto_zoom')->attr('src');
            $price = $doc->find('#price_display')->html();
            $price = empty($price)?$doc->find('#price')->html():$price;
			
			
            $price = !empty($price)?round($price * $uah_curs / $usd_curs, 2):0;
            $price = preg_replace('~[^\d\.]+~', NULL, $price);
			
            $shipping_price = $doc->find('.shippingInfo .shippingPrice')->html();
            $shipping_price = str_replace('���.', '', $shipping_price);
			
            $shipping_price =  !empty($shipping_price)?round($shipping_price * $uah_curs / $usd_curs, 2):0;
            $priceByPercent = !empty($price)?($price + $price * $percent / 100 + $shipping_price):0;
            $data = [
                'link' => $link,
                'img' => $img,
                'price' => $price,
                'product_name' => '',
                'shipping_price' => $shipping_price,
                'priceByPercent' => $priceByPercent
            ];
            return $data;
        }
    }
}