<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Products;
use app\models\Settings;
use app\api\CBRAgent;

use GuzzleHttp\Client; // lib for parsing
use yii\helpers\Url; // lib for parsing

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!empty(Yii::$app->request->post('link'))) {
            $percent = Settings::find()->where(['seting_name' => 'percent'])->one();//%
            $products = [];
            $client = new Client();
            $link = Yii::$app->request->post('link');
            //--test link start--
            //$link='https://www.amazon.com/AmazonBasics-Vinyl-Outdoor-Extension-Cord/dp/B00OS7ETIA/ref=sr_1_3?s=home-garden&srs=10112675011&ie=UTF8&qid=1498312732&sr=1-3&refinements=p_8%3A2229031011';
            //$link = 'http://www.ebay.com/itm/Smart-TV-led-43-pollici-Samsung-UE43KU6072-Ultra-HD-4K-1300hz-USB-QuadCore-Nero-/112441626027?epid=532468610&hash=item1a2e0b0dab:g:mzEAAOSwTuJYnOqX';
            //$link = 'http://www.ebay.com/itm/GoPro-HERO5-Black-Edition-64GB-SanDisk-45pcs-Mega-Loaded-Kit-Camera-Camcorder-/272457458714';
            //$link = 'https://ru.aliexpress.com/item/Original-Xiaomi-Redmi-Note-4X-3GB-16GB-Mobile-Phone-Snapdragon-625-Octa-Core-5-5-FHD/32812274215.html?spm=2114.03010108.3.1.tmgPQE&ws_ab_test=searchweb0_0,searchweb201602_1_10152_10065_10151_10068_5330012_10136_10137_10060_10155_10062_437_10154_10056_10055_10054_10059_303_100031_10099_10103_10102_10096_10052_5320015_10053_10142_10107_10050_10051_10170_10084_10083_10119_10080_10082_10081_10110_519_10111_10112_10113_10114_10182_10078_10079_5260014_10073_10123_10120_10189_10127_142_10125,searchweb201603_13,ppcSwitch_5&btsid=6e35f19d-0af7-4c6b-af1a-120fa80dc8cf&algo_expid=9ad70dba-cf67-45d3-b1b3-6b57271c3163-0&algo_pvid=9ad70dba-cf67-45d3-b1b3-6b57271c3163';
            //$link = 'https://ru.aliexpress.com/item/New-Women-s-Maxi-Long-Dubai-Dress-moroccan-Kaftan-Caftan-Jilbab-Islamic-Abaya-Muslim-Turkish-Middle/32703717978.html?spm=2114.03020108.3.1.Kmk3qT&s=p&ws_ab_test=searchweb0_0%2Csearchweb201602_4_10152_10065_10151_10068_5330012_10136_10137_10060_10155_10062_437_10154_10056_10055_10054_10059_303_100031_10099_10103_10102_10096_10052_5320015_10053_10142_10107_10050_10051_10170_10084_10083_10080_10082_10081_10110_519_10111_10112_10113_10114_10182_10078_10079_5260014_10073_10123_10189_10127_142_10125%2Csearchweb201603_9%2CppcSwitch_4_ppcChannel&btsid=8d9d940e-62fa-422b-9e4b-531328fffbbc';
            //$link = 'https://ru.aliexpress.com/item/DOOGEE-Shoot-1-2GB-RAM-16GB-ROM-5-5-inch-QHD-Android-6-0-MTK6737T-Quad/32788389527.html?spm=2114.03010108.3.31.nYuTqZ&ws_ab_test=searchweb0_0,searchweb201602_4_10152_10065_10151_10068_5330012_10136_10137_10060_10155_10062_437_10154_10056_10055_10054_10059_303_100031_10099_10103_10102_10096_10052_5320015_10053_10142_10107_10050_10051_10170_10084_10083_10080_10082_10081_10110_519_10111_10112_10113_10114_10182_10078_10079_5260014_10073_10123_10189_10127_142_10125-10127,searchweb201603_9,ppcSwitch_4&btsid=ba0b6b7c-0694-4d5c-b675-c14585fa0078&algo_expid=42569a10-e86c-424c-989f-c63750e4df44-3&algo_pvid=42569a10-e86c-424c-989f-c63750e4df44';
            //$link='https://ua-tao.com/taovar_taobao/&id=549189888806&ld=eyJwcm9tb3Rpb25fcHJpY2UiOiI5LjkwIiwidm9sdW1lIjoxNTA2OSwic2NvcmUiOjE0LCJzZWxsZXJJZCI6IjIyMjAwNjQzNDAifQ==';
            //$link='https://ua-tao.com/taovar_taobao/&id=526369567248&ld=eyJwcm9tb3Rpb25fcHJpY2UiOiI1LjkwIiwidm9sdW1lIjoxNTMyOCwic2NvcmUiOjE1LCJzZWxsZXJJZCI6IjIyNTg0NjMzMzkifQ==';
            //--test link end--
            $res = $client->request('GET', $link);
            $body = $res->getBody();
            // подключаем phpQuery
            $document = \phpQuery::newDocumentHTML($body);
            $parser_not_found = false;
            $data=null;
			
            if (strpos($link, 'https://ua-tao.com') !== false) {
                $data=Yii::$app->parser->parserTao($document, $percent->value, $link);
            } elseif (strpos($link, 'https://ru.aliexpress.com') !== false) {
                $data=Yii::$app->parser->parserAliexpress($document, $percent->value, $link);
            } elseif (strpos($link, 'http://www.ebay.com') !== false) {
                $data=Yii::$app->parser->parserEbay($document, $percent->value, $link);
            } elseif (strpos($link, 'https://www.amazon.com') !== false) {
                $data=Yii::$app->parser->parserAmazon($document, $percent->value, $link);
            } else {
                $parser_not_found = true;
            }
            if(!empty($data)){
                $this->saveProduct($data);
                $link='';
            }else{
                $parser_not_found = true;
            }
        }
        $cbr = new CBRAgent();
        if ($cbr->load()) {
            $usd_curs = $cbr->get('USD');
            $uah_curs = $cbr->get('UAH') / 10;
            $eur_curs = $cbr->get('EUR');
        } else {
            $usd_curs = 0;
            $uah_curs = 0;
            $eur_curs = 0;
        }
        $products = Products::find()->limit(10)->orderBy('date_update DESC')->all();

        return $this->render('index', [
            'parser_not_found' => $parser_not_found,
            'link' => $link,
            'products' => $products,
            'usd_curs' => $usd_curs,
            'uah_curs' => $uah_curs,
            'eur_curs' => $eur_curs,
        ]);

    }

    /**
     * save product to db.
     *
     * @param array $data. array of fields to need save to db
     *
     * @return void
     */

    protected function saveProduct($data)
    {
        if (!empty($data)) {
            $product = Products::find()->where(['link' => $data['link']])->one();
            if ($product) {
                 $product->link = $data['link'];
                 $product->image = $data['img'];
                 $product->price = $data['price'];
                 $product->product_name = $data['product_name'];
                 $product->shipping_price = $data['shipping_price'];
                 $product->percent_price = $data['priceByPercent'];
                 $product->date_update = date('Y-m-d H:i:s');
                 $product->save();
            } else {
                $product = new Products();
                $product->link = $data['link'];
                $product->image = $data['img'];
                $product->price = $data['price'];
                $product->product_name = $data['product_name'];
                $product->shipping_price = $data['shipping_price'];
                $product->percent_price = $data['priceByPercent'];

                $product->save();
            }
        }
    }

    /**
     * ---------------------------------------
     */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
